import java.io.*;
class TriangleCheck
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Enter first angle");
            int ang1=Integer.parseInt(br.readLine());

            System.out.println("Enter second angle");
            int ang2=Integer.parseInt(br.readLine());

            System.out.println("Enter third angle");
            int ang3=Integer.parseInt(br.readLine());
    
            int c=ang1+ang2+ang3;
            if(c==180)
            {
                System.out.println("The triangle with angles "+ang1+" "+ang2+"&"+ang3+" is a valid one");
            }
            else
            {
                System.out.println("The triangle with angles "+ang1+" "+ang2+"&"+ang3+" is a Invalid one!");

            }
    }
}