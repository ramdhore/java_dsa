import java.io.*;
class UpperLower{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        char c=(char)br.read();
        if(c>=65&&c<=90){
            System.out.println(c+" is in Uppercase");
        }
        else if(c>=97&&c<=122){
            System.out.println(c+" is in Lowercase");

        }
    } 
}