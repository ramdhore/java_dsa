import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the rows");
        int row=Integer.parseInt(br.readLine());
        int num=10;
        for(int i=1;i<=row;i++)
        {
            for(int space=3;space>=i;space--)
            {
                
                System.out.print("  ");
            }
            for(int j=1;j<=i;j++)
            {
                System.out.print(num*num+" ");
                num--;
            }
            System.out.println();

        }
    }
}