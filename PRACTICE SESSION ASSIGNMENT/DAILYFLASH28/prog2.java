import java.io.*;
class HexToOct
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Hex number");
        String HexNum=br.readLine();
        int OctNum=Integer.toOctalString(HexNum,8);
        System.out.println(OctNum);

    }
}
