import java.io.*;
class CalcVelocity{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Distance in meter");
        float dist=Float.parseFloat(br.readLine());

        System.out.println("Enter the time in sec");
        float time=Float.parseFloat(br.readLine());

        float velocity=dist/time;
        System.out.println("The velocity of a particle roaming in space is "+velocity+" m/s");

    }
}
