import java.io.*;
class MinNum{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the first number");
        int num1=Integer.parseInt(br.readLine());

        System.out.println("Enter the second number");
        int num2=Integer.parseInt(br.readLine());

        if(num1<num2){
            System.out.println("Minimum number amongst "+num1+" & "+num2+" is:"+num1);
        }
        else if(num1==num2){
            System.out.println(num1+" & "+num2+" both are same");

        }
        else{
            System.out.println("Minimum number amongst "+num1+" & "+num2+" is:"+num2);
        }
    }
}