import java.io.*;
class OhmLaw
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the current ");
        float current=Float.parseFloat(br.readLine());

        System.out.println("Enter the resistance ");
       float resi=Float.parseFloat(br.readLine());

        float voltage=current*resi; //v=I*R
        System.out.println("Voltage is "+voltage);
        
    }
}
