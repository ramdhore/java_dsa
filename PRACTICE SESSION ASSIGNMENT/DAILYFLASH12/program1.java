import java.io.*;
class PerfectNum
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number");
        int num=Integer.parseInt(br.readLine());
        int count=0;
        int sum=0;
        for(int i=1;i<num;i++)
        {
            if(num%i==0)
            {
                sum=sum+i;
            }
        }
        if(sum==num){
            System.out.println(num+" is a perfect Number");
        }
        else
        {
        System.out.println(num+" is not perfect Number");
        }

    }
}