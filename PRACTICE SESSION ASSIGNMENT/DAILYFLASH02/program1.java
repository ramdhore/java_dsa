import java.io.*;
class AlphaDemo{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the char input");
        
        char c=(char)br.read();
    
        
        if((c>=65&& c<=90)||(c>=97&&c<=122)){
            System.out.println(c+" is an alphabet");
        }
        else{
            System.out.println(c+" is not an alphabet");

        }
        
    }
}