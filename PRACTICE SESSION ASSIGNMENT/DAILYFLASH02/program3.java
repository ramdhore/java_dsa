import java.io.*;
class AlphaDiSp{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the char input");
        
        char c=(char)br.read();
    
        
        if((c>=65&& c<=90)||(c>=97&&c<=122)){
            System.out.println(c+" is an alphabet");
        }
        else if(c>=48&&c<=57){
            System.out.println(c+" is a digit");

        }
        else if((c>=32&&c<=47)||(c>=58&&c<=64)||(c>=91&&c<=96)||(c>=123&&c<=126)){
            System.out.println(c+" is a special character");

        }
        
    }
}