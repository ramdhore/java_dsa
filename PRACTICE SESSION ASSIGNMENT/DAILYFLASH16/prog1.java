import java.io.*;
class RevOddNum
{
    public static void main(String[]args)throws IOException
    {
        BufferedReader br=new BufferedReader(new  InputStreamReader(System.in));
        System.out.println("Enter the limit");
        int limit=Integer.parseInt(br.readLine());
        System.out.println("The odd number in reverse order are:");
        for(int i=100;i>=1;i--){
            if(i%2!=0){
        System.out.println(i+" ");

            }
        }
    }
}