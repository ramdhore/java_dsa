import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row");
        int row=Integer.parseInt(br.readLine());

        for(int i=1;i<=row;i++)
        {
            for(int j=row;j>=i;j--)
            {
                if(j%2==0)
                {
                    System.out.print("+"+" ");

                }
                else
                {
                    System.out.print("="+" ");

                }
            }
            System.out.println();

        }
    }
}