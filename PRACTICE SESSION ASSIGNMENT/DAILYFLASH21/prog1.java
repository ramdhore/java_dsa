import java.io.*;
class CheckPrime
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number");
        int num=Integer.parseInt(br.readLine());
        int count=0;
        for(int i=1;i<=num;i++)
        {
            if(num%i==0)
            {
                count++;
            }
        }
        if(count==2)
        {
            System.out.println("The number "+num+" is a prime number");
        }
        else
        {
            System.out.println("The number "+num+" is not a prime number");

        }
    }
}