import java.io.*;
class DecToHex
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the decimal number");
        int D_Num=Integer.parseInt(br.readLine());
        String H_Num=Integer.toHexString(D_Num);
        System.out.println(H_Num);
    }
}