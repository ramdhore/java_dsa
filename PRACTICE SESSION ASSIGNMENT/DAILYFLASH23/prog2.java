import java.io.*;
class OctToBin
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the octal number");
        int OctNum=Integer.parseInt(br.readLine());
        String BinNum=Integer.toBinaryString(OctNum);
        System.out.println(BinNum);
    }

}