import java.io.*;
class CountNum
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the  number ");
        int num=Integer.parseInt(br.readLine());
        int counter=0;
        int flag;
        while(num>0)
        {   
            num=num/10;
            counter++;
        }
        System.out.println("The number "+num+" has "+counter+" digits");
    }
}