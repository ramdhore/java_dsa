import java.io.*;
class CheckAbundant
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number");
        int limit=Integer.parseInt(br.readLine());
        
        String flag="";
        for(int i=1;i<=limit;i++)
        {   
            int sum=0;

            for(int j=1;j<=limit;j++)
            {
                if(i%j==0)
                {
                    sum=sum+i;
                }
            }
          
            if(sum>i)
            {
                flag+=i+" ";
            }
        }
        System.out.println("The series is:\n"+flag);
    }
}