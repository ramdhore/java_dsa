import java.io.*;
class Calculator
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the first number");
        int num1=Integer.parseInt(br.readLine());

        System.out.println("Enter the second number");
        int num2=Integer.parseInt(br.readLine());

        System.out.println("Enter the sign of Operation like +,-,*,/ ");
        char sign=(char)br.read();
        int c1=num1+num2;
        int c2=num1-num2;
        int c3=num1*num2;
        int c4=num1/num2;
        if(sign=='+')
        {
            System.out.println("Addition is:"+c1);
        }
        else if(sign=='-')
        {
            System.out.println("Subtraction is:"+c2);
        }
        else if(sign=='*')
        {
            System.out.println("Multiplication is:"+c3);
        }
        else if(sign=='/')
        {
            System.out.println("Division is:"+c4);
        }
        else
        {
            System.out.println("Invalid entry");

        }
    }
}

