import java.io.*;
class Pattern{
    public static void main(String[] args)throws IOException{
        
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row");
        int x=Integer.parseInt(br.readLine());
        System.out.println("Enter the col");
        int y=Integer.parseInt(br.readLine());
        int j=1;
        int n=x-1;//4-1=3
        for(int i=1;i<=x;){//1<=3,2<=3
            if(j<i){//1<1,1<2
                System.out.print(n+" ");//,//2
                j++;
                n++;
                continue;
            }
            if(j==i){//1==1,2==2
                System.out.println(n+" ");//3,3
                j=1;
                i++;
                n=x-i;//n=2

            }
        }
    }
}