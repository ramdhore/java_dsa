import java.io.*;
class HexToDec
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Hex number");
        String Hex_Num=br.readLine();
        int Dec_Num=Integer.parseInt(Hex_Num,16);
        System.out.println(Dec_Num);
    }
}