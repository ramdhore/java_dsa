import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the rows");
        int row=Integer.parseInt(br.readLine());
        for(int i=1;i<=row;i++)
        {
            int num=3;
            char c='D';
            for(int space=3;space>=i;space--)
            {
                System.out.print("  ");
                c-=1;
                
            }
            for(int j=i;j>=1;j--)
            {
                System.out.print(c+""+num);
                num++;

            }
            System.out.println();

        }
    }
}