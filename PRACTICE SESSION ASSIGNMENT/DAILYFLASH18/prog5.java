import java.io.*;
class DivisorSum
{   
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the  Number");
        int num=Integer.parseInt(br.readLine());
        int sum=0;
        for(int i=1;i<=num;i++)
        {
            if(num%i==0)
            {
                sum=sum+i;
                System.out.println(i+" ");
            }
        }
        System.out.println("The sum of all Possible divisors of "+num+" is:"+sum);
        
    }
}