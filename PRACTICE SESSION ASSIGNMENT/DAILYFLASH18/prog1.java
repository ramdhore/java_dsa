import java.io.*;
class PrimeSeries
{   
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the limit");
        int limit=Integer.parseInt(br.readLine());

        String PrimeNum="";
        for(int i=1;i<=limit;i++)
        {   
            int count=0;

            for(int j=1;j<=limit;j++)
            {
                if(i%j==0)
                {
                    count++;
                }
            }
          
            if(count==2)
            {
                PrimeNum+=i+" ";
            }
        }
        System.out.println("The series is:\n"+PrimeNum);
        
    }

}