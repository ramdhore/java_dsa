import java.io.*;
class Pattern{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row");
        int row=Integer.parseInt(br.readLine());

        System.out.println("Enter the col");
        int col=Integer.parseInt(br.readLine());

        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                if(i%2==0)
                {
                System.out.print("0"+" ");
                }
                else
                {
                System.out.print("1"+" ");

                }
            }
            System.out.println();

        }
    }
}
       