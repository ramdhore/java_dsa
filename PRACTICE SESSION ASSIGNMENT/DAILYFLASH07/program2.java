import java.io.*;
class IntegerSum{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the limit of integer");
        int lim=Integer.parseInt(br.readLine());
        int sum=0;
        for(int i=0;i<=lim;i++){
            sum=sum+i;
        }
        System.out.println("the sum of numbers up to "+lim+" is "+sum );
    }
}

