import java.io.*;
class SeriesSum{
    public static void main(String []args)throws  IOException  
    {
        BufferedReader br=new BufferedReader(new  InputStreamReader(System.in));
        System.out.println("Enter the Nth term Element");
        int nth_term=Integer.parseInt(br.readLine());
        System.out.println("Enter the starting element of series");
        int num_series=Integer.parseInt(br.readLine());
        int sum=num_series;
        int p=num_series;

        for(int i=1;i<nth_term;i++)
        {
            num_series=(num_series*10)+p;
            sum=sum+num_series;
            
        }
        System.out.println("The sum of :"+p+"th series ="+sum);
    }
}