import java.io.*;
class CharPattern {
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the rows");
        int row=Integer.parseInt(br.readLine());

        char c='A';
        for(int i=0;i<=row;i++){
            for(int j=row;j>=i;j--){
                System.out.print(c+" ");
            }
            c++;
            System.out.println();

        }
    }
}
