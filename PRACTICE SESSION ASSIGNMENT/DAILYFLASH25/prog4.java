import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the rows");
        int row=Integer.parseInt(br.readLine());

        for(int i=1;i<=row;i++)
        {
            int num=3;
            for(int j=3;j>=i;j--)
            {
                System.out.print(" ");
            }
            for(int k=1;k<=i;k++)
            {
                System.out.print( num*num);
                num++;

            }
            System.out.println( " ");

        }
    }
}