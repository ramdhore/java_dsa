import java.io.*;
class FiboSeries
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the limit");

        int limit=Integer.parseInt(br.readLine());

        int num1=0;
        int num2=1;
        System.out.print(num1+","+num2+",");
        for(int i=1;i<=limit-2;i++)
        {
            int num3=num1+num2;
            System.out.print(num3+",");
            num1=num2;
            num2=num3;
        }
        System.out.println();

    }
}