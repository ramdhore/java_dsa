import java.io.*;
class BreakLoop
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        
        for(int i=1;i<=10;i++)
        {   
            int num=Integer.parseInt(br.readLine());
            if(num<0){
                System.out.println("Negative Number Entered,Existing the loop");
                break;
            }
        }
        
    }
}