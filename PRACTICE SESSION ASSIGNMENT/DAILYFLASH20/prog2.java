import java.io.*;
class DecToBin
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the decimal Number");
        int D_Num=Integer.parseInt(br.readLine());
        String B_Num=Integer.toBinaryString(D_Num);
        System.out.println(B_Num);
    }
}