import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row of number");
        int row=Integer.parseInt(br.readLine());
        char c='a';
        for(int i=1;i<=row;i++)
        {
            for(int j=row;j>=i;j--)
            {
                System.out.print(c+" ");
                c++;
            }
            if(i<2){
                c-=2;
            }
           else if(i==2)
           {
                c-=1;
           }
           else
           {
               c++;
           }
            System.out.println();
            
        }
    }
}