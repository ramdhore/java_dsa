import java.io.*;
class BinToOcta
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the binary number");
        String B_Num=br.readLine();
        int D_Num=Integer.parseInt(B_Num,2);
        String Oct_Num=Integer.toOctalString(D_Num);
        
        System.out.println(Oct_Num);
    }
}