import java.io.*;
class Pattern2
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row of number");
        int row=Integer.parseInt(br.readLine());
        for(int i=1;i<=row;i++)
        {
            int num=1;
            for(int j=3;j>=i;j--)
            {

                System.out.print(" ");
                num++;
            }
            for(int k=1;k<=i;k++){
                System.out.print(num);
                num++;
            }
            System.out.println();
        }
    }
}