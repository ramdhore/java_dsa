import java.io.*;
class KineticEner{
    public static void main(String[] args)throws IOException{
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the mass in kg");
           float mass=Integer.parseInt(br.readLine());

            System.out.println("Enter the velocity in m/s");
            float velocity=Integer.parseInt(br.readLine());

            float K_E=1/2f*mass*velocity*velocity;        
        System.out.println("kinetic Energy is :"+K_E+"J");

    }
}