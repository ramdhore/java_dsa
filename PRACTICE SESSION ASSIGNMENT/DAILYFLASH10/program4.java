import java.io.*;
class Pattern{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row number");
        int row=Integer.parseInt(br.readLine());

        for(int i=0;i<row;i++){
            for(int j=0;j<=i;j++){
                if(i%2==0){
                    System.out.print("a"+" ");
                }
                else{
                    System.out.print("A"+" ");

                }
            }
            System.out.println();

        }
    }
}