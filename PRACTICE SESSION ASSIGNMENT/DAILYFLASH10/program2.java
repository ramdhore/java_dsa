import java.io.*;
class IntNum{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the first number");
        int num1=Integer.parseInt(br.readLine());
        System.out.println("Enter the second number");
        int num2=Integer.parseInt(br.readLine());
        int temp=0;
        int swap=temp;
        System.out.println("Before Swap "+num1+" "+num2);
        swap=num1;
        num1=num2;
        num2=swap;
        System.out.println("After Swap "+num1+" "+num2);


    }
}
        