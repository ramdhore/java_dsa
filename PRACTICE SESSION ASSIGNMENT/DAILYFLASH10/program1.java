import java.io.*;
class SeriesOdd{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the minimum of series");
        int min_series=Integer.parseInt(br.readLine());
        
        System.out.println("Enter the maximum of series");
        int max_series=Integer.parseInt(br.readLine());
        System.out.println("Series of Odd Number ranging  from "+min_series+"&"+max_series+" is:");
        for(int i=min_series;i<=max_series;i++){
            if(i%2!=0&&i!=0){
            System.out.print(i+" ");
            }
        }
        System.out.println();
    }
}