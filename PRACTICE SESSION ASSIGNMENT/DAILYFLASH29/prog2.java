import java.io.*;
class RevNum{
    public static void main(String[] args)throws IOException{

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number");

        int num=Integer.parseInt(br.readLine());
        int temp=num;
        int  rev=0;
        int rem=0;
        while(num>0){
            rem=num%10;
            num=num/10;
            rev=rev*10+rem;
        }
        System.out.println(rev);
       
         
    }
}