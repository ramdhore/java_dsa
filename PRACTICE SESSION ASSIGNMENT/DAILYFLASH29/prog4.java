import java.io.*;
class Pattern{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number");
        int row=Integer.parseInt(br.readLine());
        for(int i=1;i<=row;i++)
        {
            int num=5;

            for(int space=0;space<i-1;space++){
                System.out.print(" ");
                num--;
            }
            for(int k=1;k<=row-i+1;k++){
                System.out.print(num);
                num-=1;

            }
            System.out.println();

        }
    }
}