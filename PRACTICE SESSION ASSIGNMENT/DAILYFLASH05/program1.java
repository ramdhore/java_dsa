import java.io.*;
class AreaCalc{
    public static void main(String[] args)throws IOException{
    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter the radius of circle ");
    float r=Float.parseFloat(br.readLine());
    float Area=3.14f*r*r;
    System.out.println("Area of circle is:"+Area);
    }
}