import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the rows");
        int row=Integer.parseInt(br.readLine());
        
        System.out.println("Enter the columns");
        int col=Integer.parseInt(br.readLine());

        for(int i=0;i<row;i++){
            int x=2;
            for(int j=0;j<col;j++){
                System.out.print(x+" ");
                x+=2;
            }
            System.out.println();

        }
    }
}
