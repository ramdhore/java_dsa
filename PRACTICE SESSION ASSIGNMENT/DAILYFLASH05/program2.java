import java.io.*;
class UnitCalc
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the electricity units  ");
        float unit=Float.parseFloat(br.readLine());

         if(unit<=50)
        {
            System.out.println("the charge for unit: "+unit+" is Rs. 0.50/unit");
            System.out.println("You have to  Pay Rs: "+unit*0.50);

        }
        else if(unit>50 && unit<=150)
        {
            System.out.println("the charge for unit: "+unit+" is Rs. 0.75/unit");
            System.out.println("You have to  Pay Rs: "+unit*0.75);

        }
        else if(unit>150 && unit<=250)
        {
            System.out.println("the charge for unit: "+unit+" is Rs. 1.20/unit");
            System.out.println("You have to  Pay Rs: "+unit*1.20);

        }
        else
        {
            System.out.println("the charge for unit: "+unit+" is Rs. 1.50/unit");
            System.out.println("You have to  Pay Rs: "+unit*1.50);

        }
    }
}