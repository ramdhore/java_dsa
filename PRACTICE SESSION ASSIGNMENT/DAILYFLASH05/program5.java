import java.io.*;
class Asciiprnt
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Letter");
        char input=(char)br.read();

        int input1=input;       //here we have used typecasting
        System.out.println("The ASCII value of "+input+" is "+input1);
    }
}