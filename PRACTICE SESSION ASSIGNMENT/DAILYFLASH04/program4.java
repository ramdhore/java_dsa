import java.io.*;
class Pattern{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the row no");
        int row=Integer.parseInt(br.readLine());

        System.out.println("Enter the col no");
        int col=Integer.parseInt(br.readLine());

        int num=1;
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                System.out.print(num+" ");
                num++;
            }
            System.out.println();
        }
    }
}