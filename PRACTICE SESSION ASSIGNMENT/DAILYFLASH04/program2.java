import java.io.*;
class SimpleInterest{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter principal amount");
        float p=Float.parseFloat(br.readLine());
        System.out.println("Enter rate amount");
        float r=Float.parseFloat(br.readLine());
        System.out.println("Enter time period in yr ");
        float t=Float.parseFloat(br.readLine());
        float si=(p*r*t)/100;
        System.out.println("The simple interest for principal amount"+p+" at rate "+r+" time period "+t+"yr is "+si);
    }
}