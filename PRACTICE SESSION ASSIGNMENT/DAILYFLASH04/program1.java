import java.io.*;
class PythaThrm{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    
        System.out.println("Enter the first side");
        int s1=Integer.parseInt(br.readLine());
        System.out.println("Enter the second  side");
        int s2=Integer.parseInt(br.readLine());
        System.out.println("Enter the hypotenuse side");
        int s3=Integer.parseInt(br.readLine());
        
       
        int h=s1*s1+s2*s2;
        

        if(h==s3*s3){
            System.out.println("Triangle satisfies pythagorean theorem");
        }
        else{
            System.out.println("Triangle doesn't satisfies pythagorean theorem");

        }

    }
}