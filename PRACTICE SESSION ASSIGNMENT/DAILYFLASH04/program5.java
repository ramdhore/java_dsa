import java.io.*;
class Grade{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter marks for Physics");
        int phy=Integer.parseInt(br.readLine());

        System.out.println("Enter marks for Chemistry");
        int chem=Integer.parseInt(br.readLine());

        System.out.println("Enter marks for Biology");
        int bio=Integer.parseInt(br.readLine());

        System.out.println("Enter marks for Math");
        int math=Integer.parseInt(br.readLine());

        System.out.println("Enter marks for computer");
        int comp=Integer.parseInt(br.readLine());

        float total_marks=phy+chem+bio+math+comp;
        System.out.println("Total Marks:"+total_marks);
        float percentage=(total_marks*100)/500;
        System.out.println("Total Percentage:"+percentage);


        if(percentage>=90){
            System.out.println("Grade A");
          
        }
        else if(percentage>=80&&percentage<90){
            System.out.println("Grade B");
        
        }
        else if(percentage>=70&&percentage<80){
            System.out.println("Grade C");
            
        }
        else if(percentage>=60&&percentage<70){
            System.out.println("Grade D");
           
        }
        else if(percentage>=40&&percentage<60){
            System.out.println("Grade E");
           
        }
        else if(percentage<40){
            System.out.println("Grade F");
         
        }
    }
}
       

