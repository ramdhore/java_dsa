import java.io.*;
class Leap{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the year");
        float leap_yr=Integer.parseInt(br.readLine());

        if((((leap_yr%4==0)||(leap_yr%400==0))&&(leap_yr)!=0)){
            System.out.println("It's a leap year");
        }
        else if(leap_yr==0){
            System.out.println("Invalid entry!");

        }
        else{
            System.out.println("It is not a leap year");
            
        }
    }
}