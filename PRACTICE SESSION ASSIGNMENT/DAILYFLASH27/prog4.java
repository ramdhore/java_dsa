import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the rows");
        int row=Integer.parseInt(br.readLine());

        for(int i=1;i<=row;i++)
        {
            for(int space=3;space>=i;space--)
            {
                String str="#";
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++)
            {
                if(j==1)
                {
                System.out.print("#");
                }
                else
                {

                System.out.print("*");

                }
            }
            System.out.println();

        }
    }
}