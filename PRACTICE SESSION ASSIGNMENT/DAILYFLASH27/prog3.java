import java.io.*;
class AvgNum
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number");
        int num=Integer.parseInt(br.readLine());
        int sum=0,count=0,rem=0;
       while(num>0)
       {
           rem=num%10;
           sum=sum+rem;
           num=num/10;
           count++;
       }
       float avg=(float)sum/count;

       System.out.println("The average is:"+avg+" ");
      
    }
}