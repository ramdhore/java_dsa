import java.io.*;
class CountEveNum
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the  number ");
        int num=Integer.parseInt(br.readLine());
        int counter=0;
        int flag=0;
        while(num>0)
        {   
            num=num/10;
            counter++;
            if(counter%2==0)
            {
                flag++;
            }

        }
        
        
        System.out.println(num+" has "+flag+" even digits");
        
    }
}