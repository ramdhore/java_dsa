import java.io.*;
class Pattern
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the row number");
        int row=Integer.parseInt(br.readLine());

        for(int i=1;i<=row;i++)
        {
            char c='D';
            for(int j=3;j>=i;j--)
            {
                System.out.print(" ");
                c--;
            }
            for(int k=1;k<=i;k++)
            {
                System.out.print(c+"");
                c--;
            }
            System.out.println();

        }
    }
}