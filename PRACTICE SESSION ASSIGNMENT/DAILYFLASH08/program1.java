import java.io.*;
class SquareNum{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the range");
        int num=Integer.parseInt(br.readLine());
        for(int i=1;i<=num;i++){
            System.out.println("The square of "+i+" is:"+i*i);
        }
    }
}