import java.io.*;
class TableNum{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Number");
        int num=Integer.parseInt(br.readLine());

        System.out.println("The Table of "+num+" is:");
        for(int i=1;i<=10;i++){
            System.out.print(num*i+" ");
        }
            System.out.println();

    }
}