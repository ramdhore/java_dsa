import java.io.*;
class SkipOcPerf
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the limit");
        int limit=Integer.parseInt(br.readLine());
       
        for(int i=1;i<=limit;i++)
        {   
            int sum=0;
            for(int j=1;j<=i-1;j++)
            {
                if(i%j==0)
                {
                    sum=sum+j;
                }
            }
            if(sum!=i)
                {
                    System.out.print(i+",");
                }
        }
    }
}
                