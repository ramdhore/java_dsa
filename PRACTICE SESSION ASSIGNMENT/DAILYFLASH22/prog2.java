import java.io.*;
class DecToOct
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the decimal number");
        int D_Num=Integer.parseInt(br.readLine());
        String Oct_Num=Integer.toOctalString(D_Num);
        System.out.println(Oct_Num);
    }
}