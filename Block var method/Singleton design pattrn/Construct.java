class  demoConst{
    static demoConstr obj=new demoConstr();
    private demoConstr(){
        System.out.println("In constructor");
    }
    static demoConstr create{
        return obj;
    }
    class demoMain{
        public static void main(String[] args){
            demoConstr rt1=demoCostr.create();
            System.out.println(System.identityHashCode(rt1));
            demoConstr rt2=demoCostr.create();
            System.out.println(System.identityHashCode(rt2));
        }
    }
    
}