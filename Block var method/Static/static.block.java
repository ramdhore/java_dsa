class StaticDemo{
    static int x;
    static{
        x=10;
        System.out.println("In static block");//static block goes first
        
    }
     static void m1(){
        static x=10;
        System.out.println("In static method");
    }
    public static void main(String[] args)
{
    System.out.println("In main method");
    m1();
}
}