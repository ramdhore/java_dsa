class instanceDemo{
    int x=10;
    {
     
        System.out.println("In instance block1");
    }
    // instanceDemo(){
    //     x=20;
    //     System.out.println("In constructor");
    //     super();
    // }
    public static void main(String [] args){
        System.out.println("In main");
        instanceDemo obj=new instanceDemo();
        System.out.println(obj.x);
        
    }
    {
        x=20;
        System.out.println("In instance block2");
    }
}