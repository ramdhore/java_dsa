class StaticDemo{
    static int x=10;
    static{
     
        System.out.println("In static block1");
    }
    StaticDemo(){
        x=30;
        System.out.println("In constructor");
    }
  
    public static void main(String [] args){
        System.out.println("In main");
        System.out.println(x);
        StaticDemo obj=new StaticDemo();
       
    }
    static{
        x=20;
        System.out.println("In static block2");
    }
}