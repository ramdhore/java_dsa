import java.util.*;
class EveodArray{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);

        System.out.println("Enter the size of row of array");
        int m=sc.nextInt();

        int[]arr=new int[m];

        for(int i=0;i<m;i++){
            arr[i]=sc.nextInt();
        }
        int eve_sum=0;
        int odd_sum=0;
        for(int i=0;i<m;i++){
            if(arr[i]%2==0){
                eve_sum+=arr[i];
            }
            else{
                odd_sum+=arr[i];
            }
        }
        System.out.println("The sum of even no is:"+eve_sum);
        System.out.println("The sum of odd no is:"+odd_sum);
    }
}