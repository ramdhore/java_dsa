import java.util.*;
class InpuDemo{
    public static void main(String[] args){
        Scanner sc =new Scanner (System.in);

        System.out.println("Enter the size of rows");
        int m=sc.nextInt();

        System.out.println("Enter the size of column");
        int n=sc.nextInt();

        int[][] arr=new int[m][n];

        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                System.out.println("Enter the "+i+","+j+"th element of array");
                arr[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
               System.out.print(arr[i][j]);
            }
            System.out.println();
        }

    }
}