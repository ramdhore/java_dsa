package practice;

class Democlass {
    int x = 10;
    static int y = 20;

    public void fun() {
        System.out.println("In fun");

    }

    public static void run() {
        System.out.println("In run");
    }

    public static void main(String[] args) {
        Democlass obj = new Democlass();
        System.out.println(y);
        System.out.println(obj.x);
        obj.fun();
        run();
    }

}
