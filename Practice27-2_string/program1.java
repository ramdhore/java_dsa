import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
class StringFd{
    public static void  main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
        System.out.println("Enter the no of Strings");
        int n=Integer.parseInt(br.readLine());
    
        String[]sarr=new String[n];

        for(int i=0;i<n;i++){
            System.out.println("Enter the string");
            sarr[i]=br.readLine();
        }
        boolean flag=true;
        System.out.println("enter the string you want to search");
        String search=br.readLine();
        for(int i=0;i<n;i++){
            if(sarr[i].equalsIgnoreCase(search)){
                flag=true;
                System.out.println(search+" keyword found at index:"+i);
                break;
            }
            
        }
        if(flag==false){
            System.out.println("Keyword Not found");
        }
    }
}