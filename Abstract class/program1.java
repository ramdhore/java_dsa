abstract class Parent{
    void M1(){
        System.out.println("In m1 Parent");
}
public static void main(String[] args){
    // Parent p=new Parent();//here  parent is abstract sowe can not make its object
    Parent p=new Child();
    p.M1();
}
}
class Child extends Parent{
    void M1(){
        System.out.println("In m1 Child");
    }
    
}