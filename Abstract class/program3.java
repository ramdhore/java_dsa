abstract class HindustanPencils{
     abstract void Price();
}
class Natraj extends HindustanPencils{
    void Price(){
        System.out.println("Natraj is cheaper than Apsara ");
    }
}
class Apsara extends HindustanPencils{
    void Price(){
        System.out.println("Apsara is expensive  than Natraj");
    }
}
class Main{
    public static void main(String[] args){
        HindustanPencils h=new Natraj();
        h.Price();
        HindustanPencils c=new Apsara();
        c.Price();
    }
}