class Demo{
    int x=10;
    static int y=20;
    Demo(){
        System.out.println("In demo");
    }
    Demo(int z){
        System.out.println("In Demo(int)");
        System.out.println(x);
    }
    Demo(){
        System.out.println("In Demo(Demo)");
        obj.M1();
    }
    void M1(){
        System.out.println("In M1");
        System.out.println(this);

    }
    public static void main(String[] args){
        Demo obj1=new Demo();
        obj1.M1();

        Demo obj2=new Demo(10);
        obj2.M1();

        Demo obj3=new Demo(Demo(50));
        obj3.M1();
    }
}