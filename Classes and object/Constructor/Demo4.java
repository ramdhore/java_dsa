class Demo{
    int x=10;
    Demo(){
        // super();
        this(10);//here it must be written at starting of line otherwise it will throw error
        System.out.println("In constructor");
        // this(10);
        
    }
    Demo(int x){
        System.out.println("In para constructor");
        System.out.println(x);
    }
    public static void main(String [] args){
        Demo obj=new Demo();//without creatinng second obj we call another costructor
        

    }
}
