class Demo{
    int x=10;
    Demo(){
        this(10);
        this.x=20;
        System.out.println("In constructor");
        System.out.println(x);//20
    
        
    }
    Demo(int x){
        System.out.println("In para constructor");
        System.out.println(x);//10
    }
    public static void main(String [] args){
        Demo obj=new Demo();
        System.out.println(obj.x);//20
        

    }
}
