class Demo{
    Demo(){
        System.out.println("In constructor");
    }
    Demo(int x){
        System.out.println("In para constructor");
        System.out.println(x);


    }
    public static void main(String [] args){
        Demo obj1=new Demo();
        Demo obj2=new Demo(10);
        System.out.println(obj1);//Address return
        System.out.println(obj2);//Address returnDEmo

    }
}