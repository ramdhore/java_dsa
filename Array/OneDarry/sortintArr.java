import java.util.*;
import java.util.Arrays;
class SortArr{
    public static void main(String[] args){
        Scanner sc =new Scanner(System.in);
        System.out.println("Enter the size of array");
        int n=sc.nextInt();
        int[] arr=new int[n];
        for(int i=0;i<n;i++){
            System.out.println("Enter the "+i+"th index element");
            arr[i]=sc.nextInt();
        }
        
        for(int i=0;i<4;i++){
            Arrays.sort(arr);

            System.out.println(arr[i]);
                
            }

    }
}