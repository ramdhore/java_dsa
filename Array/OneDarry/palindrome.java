import java.io.*;
class ioDemo{
    public static void main(String[] args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of array");
        int n=Integer.parseInt(br.readLine());
       

        int[]arr=new int[n];

        for(int i=0;i<n;i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        for(int i=0;i<n;i++){
            int num=arr[i];
            int rem;
            int rev=0;

            while(num>0){
            rem=num%10;
            num=num/10;
            rev=rev*10+rem;
            }
            if(arr[i]==rev){
                System.out.println(arr[i]+"No is palindrome at:"+i);
            }
            else{
                System.out.println(arr[i]+"No is not palindrome at:"+i);
    
            }

        }
        
    }
}