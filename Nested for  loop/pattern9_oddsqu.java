class OddSqu {
    public static void main(String[] args) {
        int count = 1;

        for (int row = 1; row <= 3; row++) {//r=1 r=2
            for (int col = 1; col <= 3; col++) {
                if ((row + col) % 2 == 0) {  //2%2==0   //3%2!=0    //4%2==0    3%2!=0
                    System.out.print(count + " ");//1 //2

                } else {
                    int x = count * count;

                    System.out.print(x + " ");//1 //
                }
                count++;//2

            }
            System.out.println();

        }
    }
}