interface Indian_Culture{
    void Lifestyle();
    
    void Festival();
}
class Maharashtra implements Indian_Culture{
    public void Lifestyle(){
        System.out.println("Sari");
    }
    public void Festival(){
        System.out.println("Ganeshotsav");
    }
}
class Kerala implements Indian_Culture{
   public void Lifestyle(){
        System.out.println("Lungi");
    }
    public void Festival(){
        System.out.println("Onam");
    }
}
class Culture{
    public static void main(String[]args){
       Maharashtra M=new Maharashtra();
        M.Lifestyle();
        M.Festival();
        Kerala IC2=new Kerala();
        IC2.Lifestyle();
        IC2.Festival();
    }
}