class A{
    final static int x=10;
}
interface B{
    final static int x=20;
}
class C extends A implements B{

    public static void main(String[] args){
    System.out.println(A.x);
    System.out.println(B.x);
    }
    
}