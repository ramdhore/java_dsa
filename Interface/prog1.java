interface A{
    public static final int x=10;       //here 10 only comes in picture when we are trying to  access
     void M1();        //Here it will add public abstract
        // System.out.println("In M1 A")
}
class B implements A{
    // void M1(){          //It will throw error bcz access scope reduced here
    public void M1(){
        System.out.println("In M1");
        System.out.println(x);
        System.out.println(this);
    }
}
class Demo{
    public static void main(String[] args){
        A obj=new B();
        obj.M1();
        System.out.println(obj);

    }
}