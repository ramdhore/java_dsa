import java.io.*;
class SumArray{
    public static  void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of array ");
        int n=Integer.parseInt(br.readLine());
        int[] arr=new int[n];

        for(int i=0;i<n;i++){
            System.out.println("Enter the "+i+"th index element");
            arr[i]=Integer.parseInt(br.readLine());
        }
        int sum=0;
        for(int i=0;i<n;i++){
            sum+=arr[i];
        }
        System.out.println("The sum of array is:"+sum);
    }
}