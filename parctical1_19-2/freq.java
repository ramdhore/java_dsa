import java.io.*;
class FreqArray{
    public static  void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of array ");
        int n=Integer.parseInt(br.readLine());
        int[] arr=new int[n];

        for(int i=0;i<n;i++){
            System.out.println("Enter the "+i+"th index element");
            arr[i]=Integer.parseInt(br.readLine());
        }

        for(int i=0;i<n;i++){
            int flag=0;
            int count=0;
            for(int k=i-1;k>0;k--){
                if(arr[i]==arr[k]){
                    flag=1;
                    break;
                }
            }
            if(flag==0){
                for(int j=0;j<n;j++){
                    if(arr[i]==arr[j]){
                        count++;
                    }
                }
            }
            if(flag==0){
                System.out.println("The frequency of "+arr[i]+" is "+count);
            }
        }
    }
}