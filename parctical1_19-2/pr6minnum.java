import java.io.*;
class MinDemo{
    public static  void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of array ");
        int n=Integer.parseInt(br.readLine());
        int[] arr=new int[n];

        for(int i=0;i<n;i++){
            System.out.println("Enter the "+i+"th index element");
            arr[i]=Integer.parseInt(br.readLine());
        }
        int min=10000000;
        for(int i=0;i<n;i++){
        if(arr[i]<min){
                min=arr[i];
            }
        }
        System.out.println("The min element is:"+min);


    }
}