import java.io.*;
class FreqArray{
    public static  void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of array ");
        int n=Integer.parseInt(br.readLine());
        int[] arr=new int[n];

        for(int i=0;i<n;i++){
            System.out.println("Enter the "+i+"th index element");
            arr[i]=Integer.parseInt(br.readLine());
        }
        boolean[] visited=new boolean[n];
        for(int i=0;i<n;i++){
            if(visited[i]==true){
                continue;
            }
            int count=1;
            
            for(int j=i+1;j<n;j++){
            if(arr[i]==arr[j]){
                visited[j]=true;
              count++;
            }
        }
        System.out.println("The frequency of"+arr[i]+"is"+count);

    }

    }
}