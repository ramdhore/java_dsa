import java.io.*;
class StrongArray{
    public static  void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the size of array ");
        int n=Integer.parseInt(br.readLine());
        int[] arr=new int[n];

        for(int i=0;i<n;i++){
            System.out.println("Enter the "+i+"th index element");
            arr[i]=Integer.parseInt(br.readLine());
        }
        int sum=0;
        int rem=0;
        for(int i=0;i<n;i++){
            int num=arr[i];
            rem=num%10;
            num=num/10;
            rem=rem*i;
            sum+=num;
        }
        System.out.println("The sum of array is:"+sum);
    }
}