class DaemonDemo extends Thread{
    public void run(){
        String tName=Thread.currentThread().getName();
        for(int i=0;i<10;i++){
            System.out.println("In run "+tName);
        }
    }
}
class Demo{
    public static void main(String[] args){
        DaemonDemo obj1=new DaemonDemo();
        DaemonDemo obj2=new DaemonDemo();
        obj1.setDaemon(true);
        obj1.start();
        obj2.start();
    }
}