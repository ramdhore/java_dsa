class YieldDemo extends Thread{
    public void run(){
        for(int i=0;i<10;i++){
            System.out.println("In run");
        }
        try{
            Thread.sleep(2000);
        }catch(InterruptedException ie){

        }
    }
}
class Demo{
    public static void main(String[] args){
        YieldDemo obj=new YieldDemo();
        obj.start();
        obj.yield();
        for(int i=0;i<10;i++){
            System.out.println("In main");
        }
        try{
            Thread.sleep(2000);
        }catch(InterruptedException ie){}
    }
}