import java.io.*;
class ThreadDemo2 extends Thread{
    void fun(){
        System.out.println("In fun by:");
        System.out.println(Thread.currentThread().getName());
        
    }
    void gun(){
        System.out.println("In gun by:");
        System.out.println(Thread.currentThread().getName());
    }
    public void run()
    {
        if(Thread.currentThread().getName().equals("Thread-0"))
        {
                fun();
        }
        if(Thread.currentThread().getName().equals("Thread-1"))
        {
                gun();
        }

    }
    public static void main(String[]args){
        ThreadDemo td1=new ThreadDemo();
        td1.start();

        ThreadDemo td2=new ThreadDemo();
        td2.start();
    }
}