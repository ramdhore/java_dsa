class MyException extends Exception
{
    MyException(String msg)
    {
        super(msg);
    }
}
class ThreadDemo1 extends Thread 
{ 
   
    void fun()throws MyException
    {
        System.out.println("In fun");
        throw new MyException("MyArithmeticException");

    }
    public void run()
    {
        System.out.println("In run"+Thread.currentThread().getName());
        try
        {
            throw new MyException("ArithmeticException");
        }catch(MyException me){}
        ThreadDemo2 obj1=new ThreadDemo2();
        Thread obj=new Thread(obj1);
    }
}
class ThreadDemo2 implements Runnable
{
   
    void fun()throws MyException
    {
        System.out.println("In fun"+Thread.currentThread().getName());
    }
        
        // obj1=null;
    public void run()
    {
        System.out.println("In fun"+Thread.currentThread().getName());
            try
            {
            throw new MyException("Null pointer exception");
            }catch(MyException me){}
    }
}
class Demo{
    public static void main(String[] args){
        ThreadDemo1 obj2=new ThreadDemo1();
        obj2.start();
        for(int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName());
        }
    }
}