class MyException extends Exception{
    MyException(String msg){
        super(msg);
    }
}
class ThreadDemo1 implements Runnable{
    void fun()throws MyException{
        System.out.println("In fun"+10/0);
        throw new MyException("ArithmeticException");
    }
    public void run(){  

        System.out.println("In run having 20 intermediate"+Thread.currentThread().getName());
        try{
            throw new MyException("ArithmeticException");
        }catch(MyException me){
            me.printStackTrace();
        }

    }
}
class ThreadDemo2 extends Thread{
    void fun(){
        System.out.println("In fun"+Thread.currentThread().getName());

    }
    public void run(){
        ThreadDemo1 obj=new  ThreadDemo1();
        Thread t=new Thread(obj);
        t.setName("Biencaps");
        t.start();
        for(int i=1;i<50;i++){
            if(i%7==0){
            System.out.println("In run"+Thread.currentThread().getName());
            }
        }
    }
}
class ThreadDemo{
    public static void main(String[] args){
        ThreadDemo2 obj1=new ThreadDemo2();
        obj1.setName("C2W");
        obj1.start();

        for(int i=0;i<10;i++){
            System.out.println("In main");
        }
    }
}