import java.io.*;
class MyException extends Exception{
    MyException(String msg){
    super(msg);
    }
}
class ThreadDemo extends Thread{
    void fun()throws IOException{
        System.out.println("In fun by:");
        System.out.println(Thread.currentThread().getName());
        throw new IOException("IOException");
    }
    void gun()throws MyException{
        System.out.println("In gun by:");
        System.out.println(Thread.currentThread().getName());
        throw new MyException("MyException");
    }
    public void run(){
        if(Thread.currentThread().getName().equals("C2W")){
            try{
                fun();
            }
            catch(IOException ie){
                ie.printStackTrace();
            }
        }
            
        else if(Thread.currentThread().getName().equals("Biencaps")){
            try{
                gun();
            }
            catch(MyException me){
                me.printStackTrace();

            }

        }
    }
    public static void main(String[]args){
        ThreadDemo td1=new ThreadDemo();
        td1.setName("C2w");
        td1.start();

        ThreadDemo td2=new ThreadDemo();
        td2.setName("Biencaps");
        td2.start();
    }
}