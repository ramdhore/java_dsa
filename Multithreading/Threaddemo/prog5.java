class ThreadDemo1 extends Thread{
    void  fun(){
        System.out.println("In fun");
    }
    public void  run(){
        System.out.println("In run");
        ThreadDemo2 obj=new ThreadDemo2();
        Thread t=new Thread(obj);
        t.setName("Creation");
        t.start();
        System.out.println(Thread.currentThread().getName());
    }
}
class ThreadDemo2 implements Runnable{
    void  fun(){
        System.out.println("In fun");
    }
    public void  run(){
        System.out.println("In run "+Thread.currentThread().getName());

    }
}
class ThreadDemo{
    public static void main(String[] args){
        ThreadDemo1 obj1=new ThreadDemo1();
        obj1.setName("Rj");
        // obj1.setPriority(0);
        obj1.start();
        for(int i=0;i<10;i++){
        System.out.println(Thread.currentThread().getName());
        }
    }
}