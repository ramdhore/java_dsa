class ThreadDemo extends Thread{
    ThreadDemo(){
        super();
    }
    ThreadDemo(ThreadGroup tg,String str){
        super(tg,str);
    }
}
class ThreadGroupDemo{
    public static void main(String[] args){
        ThreadDemo t1=new  ThreadDemo();
        t1.start();
        System.out.println(t1.getThreadGroup());

        ThreadGroup tg1=new ThreadGroup("c2w group");
        ThreadDemo t2=new ThreadDemo(tg1,"Ram");
        System.out.println(t2.getThreadGroup());

        ThreadGroup tg2=new ThreadGroup("Biencaps group");
        ThreadDemo t3=new ThreadDemo(tg2,"Raj");
        System.out.println(t3.getThreadGroup());

    }
}