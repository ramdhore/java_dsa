class ThreadDemo extends Thread{
    ThreadDemo(){
        super();
    }
    ThreadDemo(ThreadGroup tg,String str){
        super(tg,str);
    }
    public void run(){
        System.out.println(Thread.currentThread().getName());
        System.out.println(":In Run");
    }
}
 class ThreadGroupDemo3{
    public static void main(String[] args){
        Thread t1=new  Thread();
        t1.start();

        ThreadGroup tg1=new ThreadGroup("c2w group");
        ThreadDemo t2=new ThreadDemo(tg1,"tfirst");
        // tg1.interrupt();

        t2.start();
        ThreadDemo t3=new ThreadDemo(tg1,"tsecond");
        t3.start();
        ThreadDemo t4=new ThreadDemo(tg1,"tthird");
        t4.start();
        if(tg1.isDaemon()==true){
            System.out.println("its Daemon group");
        }
        else{
            System.out.println("its  not Daemon group");
        }
        // tg1.list();
        
        // System.out.println(t1.getThreadGroup());
        // tg1.start();//Error this method doesn't exist in threadgroup

        // ThreadGroup tg2=new ThreadGroup(tg1,"Biencaps group");
        // ThreadDemo t5=new ThreadDemo(tg2,"tfour");
        // ThreadDemo t6=new ThreadDemo(tg2,"tfive");
        // ThreadDemo t7=new ThreadDemo(tg2,"tsix");
    //    t2.start();
    //    t4.start();
    // //    t3.start();
    // //    tg1.interrupt();
    //     tg1.stop();
    //     t3.start();
    }
}