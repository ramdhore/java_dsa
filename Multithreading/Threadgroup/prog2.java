class ThreadDemo extends Thread implements Runnable{
    public void run(){
        System.out.println("In run");
    }
    public static void main(String[] args){
        Thread t1=new Thread();
        System.out.println(t1.getName());
        System.out.println(t1.getThreadGroup());
        t1.start();

        Thread t2=new  Thread("c2w");
        System.out.println(t1.getName());
        System.out.println(t2.getThreadGroup());
        t2.start();

        ThreadDemo obj=new ThreadDemo();
        Thread t3=new Thread(obj);
        System.out.println(t3.getName());
        System.out.println(t3.getThreadGroup());
        t3.start();

        ThreadDemo obj1=new ThreadDemo();
        Thread t4=new Thread(obj1,"Biencaps");
        System.out.println(t4.getName());
        System.out.println(t4.getThreadGroup());
        t4.start();



    }
}