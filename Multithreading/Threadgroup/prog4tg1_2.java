class ThreadDemo extends Thread{
    ThreadDemo(){
        super();
    }
    ThreadDemo(ThreadGroup tg,String str){
        super(tg,str);
    }
}
 class ThreadGroupDemo2{
    public static void main(String[] args){
        ThreadDemo t1=new  ThreadDemo();
        // t1.start();
        // System.out.println(t1.getThreadGroup());

        ThreadGroup tg1=new ThreadGroup("c2w group");
        ThreadDemo t2=new ThreadDemo(tg1,"tfirst");
        ThreadDemo t3=new ThreadDemo(tg1,"tsecond");
        ThreadDemo t4=new ThreadDemo(tg1,"tthird");
        // System.out.println(t1.getThreadGroup());

        ThreadGroup tg2=new ThreadGroup(tg1,"Biencaps group");
        ThreadDemo t5=new ThreadDemo(tg2,"tfour");
        ThreadDemo t6=new ThreadDemo(tg2,"tfive");
        ThreadDemo t7=new ThreadDemo(tg2,"tsix");
        // System.out.println(t7.getThreadGroup());
        System.out.println(tg1.getParent().activeGroupCount());     //two method to find  out group count
        System.out.println(Thread.currentThread().getThreadGroup().activeGroupCount());


    }
}