class ThreadDemo8 extends Thread{
    ThreadDemo8(){
        super();
    }
    ThreadDemo8(ThreadGroup tg,String str){
        super(tg,str);
    }
    public  void run(){
        System.out.println("In run");
    }
    public static void main(String[] args){
        ThreadDemo8 t1=new  ThreadDemo8();
        t1.start();
        System.out.println(t1.getThreadGroup());

        ThreadGroup tg1=new ThreadGroup("c2w group");
        ThreadDemo8 t2=new ThreadDemo8(tg1,"Ram");
    }
}