class ClassDemo{
    int x=10;
    static int y=20;
    String str1="Ram";
    String str2=new String ("Ram");

    void method1(){
        System.out.println("In method1");
        System.out.println(System.identityHashCode(x));

    }
    static void method2(){
        System.out.println("In method2");
        System.out.println(System.identityHashCode(y));

    }
    public static void main(String[] args){
        ClassDemo obj=new ClassDemo();
        obj.method1();
        obj.method2();
    } 
}