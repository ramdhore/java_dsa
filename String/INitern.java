class InternDemo{
    public static void  main(String[] args){
        String str1="core2web";
        String str2=new String ("core2web");
        System.out.println(System.identityHashCode(str1));
        System.out.println(System.identityHashCode(str2));

        String str3=str2.intern();
        System.out.println("After using intern method:)");
        System.out.println(System.identityHashCode(str1));
        System.out.println(System.identityHashCode(str3));
    }
}