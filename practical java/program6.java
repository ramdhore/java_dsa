class switchDemo {
    public static void main(String[] args) {
        int var1 = 6;

        switch (var1) {
            case 1: {
                System.out.println("One");
                break;
            }
            case 2: {
                System.out.println("Two");
                break;
            }
            case 3: {
                System.out.println("Three");
                break;
            }
            case 4: {
                System.out.println("Four");
                break;
            }
            case 5: {
                System.out.println("Five");
                break;
            }
            case 6: {
                if (var1 > 5)
                    System.out.println(var1 + " is greater than 5");
                break;
            }
            default: {
                System.out.println("enter number is default");

            }
        }
    }

}
