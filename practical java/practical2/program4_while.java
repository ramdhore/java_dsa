package practical2;

class DivProg {
    public static void main(String... args) {
        int i = 1;
        while (i <= 20) {
            if ((i % 4 == 0) && (i % 5 == 0)) {
                // System.out.println(i);
                System.out.println(i + " is divisible by 4 and 5");

            }
            i++;
        }
    }

}
