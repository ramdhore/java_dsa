package practical2;
class SumProg {
    public static void main(String... args) {
        int sum = 0;
        int i = 10;
        while (i >= 1) {
            {
                sum += i;
            }
            i--;
        }
        System.out.println("Sum of 10 to 1 is " + sum);

    }

}
