package practical2;

class DivProg {
    public static void main(String... args) {
        for (int i = 1; i <= 20; i++) {
            if ((i % 4 == 0) && (i % 5 == 0)) {
                System.out.println(i + " is divisible by 4 and 5");
                
                // System.out.println(i);
            }
        }
    }

}
