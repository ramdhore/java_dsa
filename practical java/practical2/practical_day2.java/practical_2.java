import java.lang.*;

class CostSell {
    public static void main(String[] args) {
        int sell_p = 200;
        int cost_p = 1000;
        int profit = 0;
        int loss = 0;

        if (sell_p > cost_p) {
            profit = sell_p - cost_p;
            System.out.println("Your profit is Rs:" + profit);
        } else {
            loss = cost_p - sell_p;
            System.out.println("Your loss is Rs:" + loss);

        }
    }
}