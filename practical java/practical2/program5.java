package practical2;

class SumProg {
    public static void main(String... args) {
        int sum = 0;
        for (int i = 10; i >= 1; i--) {
            {
                sum += i;
            }
        }
        System.out.println("Sum of 10 to 1 is " + sum);

    }

}
