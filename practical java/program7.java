class month {
    public static void main(String[] args) {
        int month_no = 7;

        switch (month_no) {
            case 1: {
                System.out.println("jan has 31 days");
                break;
            }
            case 2: {
                System.out.println("Feb has 28/29 days");
                break;
            }
            case 3: {
                System.out.println("march has 31 days");
                break;
            }
            case 4: {
                System.out.println("April has 30 days");
                break;
            }
            case 5: {
                System.out.println("May has 31 days");
                break;
            }
            case 6: {
                System.out.println("jun has 30 days");
                break;
            }
            case 7: {
                System.out.println("july has 31 days");
                break;
            }
            case 8: {
                System.out.println("Aug has 31 days");
                break;
            }
            case 9: {
                System.out.println("Sep has 30 days");
                break;
            }
            case 10: {
                System.out.println("Oct has 31 days");
                break;
            }
            case 11: {
                System.out.println("Nov has 30 days");
                break;
            }
            case 12: {
                System.out.println("Dec has 31 days");
                break;
            }
            default: {
                System.out.println("default entry");
                break;
            }
        }
    }
}
