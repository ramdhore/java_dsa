class TATA{
    int Model_no=43;
    static String brand_name1="Swift";
    TATA(){
        System.out.println("In TATA constructor brand");
    }
    TATA(int no){
        System.out.println("In TATA(int) constructor brand");
    }
    void Speed1(){
        System.out.println("Speed is 200km/hr");
    }
    static void Speed2(){
        System.out.println("Speed is 100km/hr");
        System.out.println(System.identityHashCode(brand_name1));

    }
}
class Nexon extends TATA{
    int Model_no=50;
    static String brand_name2="nexoncars";
    Nexon(){
        System.out.println("In NEXON constructor brand");
    }
    Nexon(int num){
        System.out.println("In NEXON(int) constructor brand");
    }
    void Speed3(){
        System.out.println("Speed is 120km/hr");
    }
    static void Speed4(){
        System.out.println("Speed is 100km/hr");
        System.out.println(System.identityHashCode(brand_name2));


    }
    }
class DEMO{
        public static void main(String[] args){
            Nexon obj1=new Nexon();
            obj1.Speed3();
            TATA obj2=new TATA();
            obj2.Speed1();
            TATA.Speed2();
            Nexon.Speed4();
            
        } 
    }