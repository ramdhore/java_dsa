class Parent{
    int x=10;
    static int y=20;
    static{
        System.out.println("In static of p");
        System.out.println(y);
    }
    static void M1(){
        System.out.println("In M1");
    }
    void M2(){
        System.out.println("In M2");
    }
    
}
class Child{
    Parent P=new Parent();

}
class Demo{
    public static void main(String[] args){
        Child c=new Child();
        c.P.M1();
    }
}