class Parent{
    int a=10;
    static int b=20;
    Parent(){
        int c=30;

        System.out.println("In Parent constr");
    }
    static{
        System.out.println("In static p");

    }
   
}
    class Child1 extends Parent{
    Child1(){
        System.out.println("In child1 constr");
    }
    static{
        System.out.println("In static c");
    
    }
    void M1(){
        int c=12;
        System.out.println("In M1");

    }
}
    class Child2 extends Parent{
        Child2(){
            System.out.println("In child2 constr");
        }
        static{
            System.out.println("In static c");
        
        }
}
class Demo{
    public static void main(String[] args){
        Child c=new Child();
        c.M1();
        System.out.println(Parent.b);
    }
}

