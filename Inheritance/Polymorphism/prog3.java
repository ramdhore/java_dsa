class Parent{


}
class Child extends Parent{

}
class Demo
{
    void M1(int x,char y){
    System.out.println("In M1 float");
}
    void M1(char x,int y){
    System.out.println("In M1 int");

}
//     void M1(float x,int y){
//     System.out.println("In M1 float");
// }
//     void M1(int x,float y){
//     System.out.println("In M1 int");

// }
    public static void main(String [] args){
        Demo d=new Demo();
    d.M1(12,14);//Always priority given to child first
    // d.M1(10.5);//no suitable method for double
    d.M1(13,12);//Ambiguous error
    d.M1('c',30);//ambiguous error
}
}