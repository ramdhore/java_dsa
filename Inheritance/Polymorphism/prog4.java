class Parent{
    void p1(){
        System.out.println("In P1");
    }
}
class Child extends Parent{
    void p2(){
        System.out.println("In P2");
    }
}
class Demo{
    void M1(Parent p){
        System.out.println("In M1 child");
    }
    void M1(Child c){
        System.out.println("In M1 child");
        c.p1();
        c.p2();
    }
    public static void main(String[] args){
        Demo d=new Demo();
        d.M1(new Child());
    }
}