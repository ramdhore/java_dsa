class Parent{
    int a=10;
    static int b=20;
    Parent(){
        int c=30;

        System.out.println("In Parent constr");
        System.out.println(this);
    }
    static{
        System.out.println("In static p");

    }
   
}
    class Child1 extends Parent{
    Child1(){
        System.out.println("In child1 constr");
        System.out.println(this);
    }
    static{
        System.out.println("In static c1");
    
    }
}
    class Child2 extends Child1{
    Child2(){
        System.out.println("In child2 constr");
        System.out.println(this);
    }
    static{
        System.out.println("In static c2");
    
    }
    }
class DemoMain{
    public static void main(String[] args){
        Child2 c=new Child2();
        
        
    }
}

