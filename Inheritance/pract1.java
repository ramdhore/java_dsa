class Home{
    int mobile=11;
    static int Tv=22;
    static{
        int Ac=23;
        System.out.println("Everyone watching tv");
        System.out.println(Ac);
    }
    {
        System.out.println("In instace Block");
        System.out.println(mobile);
    }
    Home(){
        this(10);
        System.out.println("In Home constr");
    }
    Home(int remote){
        System.out.println("In Home(int) constr");
    }
    void watching(){
        int Lock=1234;
        System.out.println("In instance watching method");
        System.out.println(Lock);
    }
   static void Cooking(){
        System.out.println("In static watching method");
    }
    public static void main(String[] args){
       Home obj=new Home();
       obj.watching();
       obj.Cooking();
    
    }
    
}