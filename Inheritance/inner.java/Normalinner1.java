class Outer{
    int x=10;
    static int y=20;
    class Inner{
        int x=30;
        void M1(){
            System.out.println("In inner M1");
        }
    }
}
class Demo{
    public static void main(String[] args){
        Outer o=new Outer();
        Outer.Inner obj=o.new Inner();
        obj.M1();
    }
}