class XYZ{
    XYZ(int x){
        System.out.println("int");
    }
    XYZ(XYZ obj){
        System.out.println("obj");
    }

    public static void main(String[] args){
        XYZ obj1=new XYZ(10);
        XYZ obj2=new XYZ(obj1);
    }
}