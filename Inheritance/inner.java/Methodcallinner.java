class Outer{
    class Inner{
        int x=10;
        void M1(){
            System.out.println(this.x);
        }
    }
    static void M2(){
        Inner i1=new Inner();
        i1.M1();
    }
    public static void main(String[] args){
        Outer o1=new Outer();
    }
}