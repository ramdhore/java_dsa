class Outer {
    int x=10;
    static  int  y=20;
    void M1(){
        class Inner{
            void M2(){
                System.out.println("In M2");
            }
        }
        Inner i=new  Inner();
        i.M2();
        System.out.println("In M1");
    }
    public static  void main(String[] args){
        Outer o=new Outer();
        o.M1();
    }
}