import java.util.TreeSet;
class Employee implements Comparable<Employee>{
    String name=null;
    // Double sal=0.0;
    double sal=0.0;
    Employee(String name,double sal)
    {
        this.name=name;
        this.sal=sal;

    }
    public int compareTo(Employee obj2){
        // System.out.println("Name= "+name+" Name2="+obj2.name);
        // return name.equals(obj2.name);
        return name.compareTo(obj2.name);
        // return sal.compareTo(obj2.sal);
        // return 0;
    }
    public String toString(){
        return name+":"+sal;    //camparision with address and return string 
    }
}
class TreeSetDemo2
{
    public static void main(String[] args)
    {

        TreeSet obj=new TreeSet();
        obj.add(new Employee("Kanha",45.65));
        obj.add(new Employee("Rahul",55.65));
        obj.add(new Employee("Ashish",35.65));
        System.out.println(obj);
    }

}