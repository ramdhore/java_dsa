import java.util.*;
class TreeSetDemo
{
    public static void main(String[] args)
    {
        TreeSet ts=new TreeSet();
        ts.add(15);
        ts.add(5);
        // ts.add("Kanha"); classcastexception Error at runtime ass we can't campare with int value
        ts.add(5);
        ts.add(10);
        ts.add(3);
        
        System.out.println(ts);
    }
}
