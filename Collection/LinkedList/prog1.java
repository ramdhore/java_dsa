import java.util.*;
class Friends{
    String Name;
    String Cname;
    float sal;
    Friends(String Name,String Cname,float sal){
        this.Name=Name;
        this.Cname=Cname;
        this.sal=sal;
    }
    void display(){
        System.out.println("{"+Name+":"+Cname+":"+sal+"}");
    }
}

class LinkedListDemo{
    public static void main(String[] args){
    LinkedList<Friends> al=new LinkedList<Friends>();
   
    al.add(new Friends("Rahul","BMC",30.00f));
    al.add(new Friends("BAdhe","Aws",12.00f));

    Iterator<Friends>itr=al.iterator();
    while(itr.hasNext()){
        itr.next().display();
    }

    }
   
    
}