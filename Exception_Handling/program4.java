import java.util.*;
import java.io.*;
class Demo{
    public static void main(String[] args){
        String str=null;
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.println("In try");
            int x=Integer.parseInt(br.readLine());
        }
        catch(IOException obj){
            System.out.println("In catch");
            Scanner sc=new Scanner(System.in);
            str=sc.nextLine();
        }
        catch(NumberFormatException e){
            System.out.println("Number format exception ");
        }
        System.out.println(str);
        System.out.println("After try-catch");
    }
}