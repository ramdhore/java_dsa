import java.io.*;
class Demo{
    public static void main(String[] args){
    
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String str=null;
        try{
            System.out.println("In try");
            int x=Integer.parseInt(br.readLine());
        }
        catch(IOException obj){
            System.out.println("In catch");
            System.out.println(obj);
           }
        catch(InterruptedException ie){     //when such type exception are not thrown by code we can't write 
            System.out.println("In catch 1");
            System.out.println(ie);
        }
        catch(NumberFormatException nf){
            System.out.println("In catch2");
            System.out.println(nf);
        }
        // System.out.println(str);
        System.out.println("After try-catch");
    }
}