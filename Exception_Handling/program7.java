import java.io.*;
class FinallyDemo{
    public static void main(String[] args){
        BufferedReader br1=new BufferedReader(new  InputStreamReader(System.in));
        BufferedReader br2=new BufferedReader(new  InputStreamReader(System.in));
        try{
            System.out.println("In try");
            br1.close();
            br1.readLine();
        }
        catch(IOException ie){
            System.out.println("In catch");
        }
        finally
        {
            try
            {
                br2.close();
                br2.readLine();
            }
            catch(Exception e )
            {
                System.out.println("catch finally");
            }
            System.out.println("In finally");

        }
    }
}