class MyException extends Exception{
    MyException(String str){
        super(str);
    }
}
class Demo{
    void M1()throws MyException{
        for(int i=5;i>0;i--){
            System.out.println(i);
            if(i==2){
                throw new MyException("Vedya divide by 2");
            }
        }
    }
    public static void main(String[] args)throws MyException{
        Demo d=new Demo();
        d.M1();
    }
}