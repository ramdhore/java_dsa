import java.io.*;
class ExceptionDemo{
    static void gun() throws IOException{
    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
        System.out.println("In gun method");
    }

    static void Fun()throws IOException{
        gun();
        System.out.println("In gun method");

    }
    public static void main(String[] args)throws IOException{
        Fun();
        System.out.println("In main method after fun");

    }
}