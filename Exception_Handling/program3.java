import java.io.*;
class Demo{
    void M1(){
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        try{
            br.close();
            br.readLine();
        }
        catch(IOException ie){
            System.out.println("Stream Closed");
        }
    }
    void M2(){
        System.out.println("Data stored in database");

    }
    public static void main(String[] args){
        Demo d=new Demo();
        d.M1();
        d.M2();
        System.out.println("At Home");
    }
}
